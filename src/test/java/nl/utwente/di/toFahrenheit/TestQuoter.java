package nl.utwente.di.toFahrenheit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests the Quoter
 */
public class TestQuoter {
    @Test
    public void testCelsius0() throws Exception {
        Quoter quoter = new Quoter();
        double celsius = quoter.getFahrenheit(Integer.valueOf("0"));
        Assertions.assertEquals(32.0, celsius, 0.0, "Fahrenheit of Celsius 0");
    }
}